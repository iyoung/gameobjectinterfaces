// Vector2d.cpp
// 2D vector class
// Your name here!

#include "Vector2d.h"
#include <cmath>

// Ive done one of the methods for you:

// Scale up. Multiply by s 
const Vector2d& Vector2d::operator*= ( const float& s ) { 
	x*=s; 
	y*=s; 
	return *this;
}
//assignment operator
const Vector2d& Vector2d::operator= ( const Vector2d& v )
{
	x=v.x;
	y=v.y;
	return *this;
}
//returns a vector based on addition of v to current. does not change current
const Vector2d Vector2d::operator+ ( const Vector2d& v ) const
{
	return Vector2d(x+v.x, y+v.y);
}
//adds v to current. returns reference to current
const Vector2d& Vector2d::operator+= ( const Vector2d& v )
{
	x+=v.y;
	y+=v.y;
	return *this;
}
//subtracts v from current, returns reference to current
const Vector2d& Vector2d::operator-=( const Vector2d& v )
{
	x-=v.x;
	y-=v.y;
	return *this;
}
//returns a vector based on subtraction of v from current. current unchanged
const Vector2d Vector2d::operator- ( const Vector2d& v ) const
{
	return Vector2d(x-v.x,y-v.y);
}
//scale vector down. return reference to current
const Vector2d& Vector2d::operator/=( const float& s )
{
	x/=s;
	y/=s;
	return *this;
}
//returns a vector based on a division of current by s. current unchanged
const Vector2d Vector2d::operator/ (float s) const
{
	return Vector2d(x/s,y/s);
}
//returns negative of this vector
Vector2d Vector2d::operator-()
{
	return Vector2d(-x,-y);
}
// vector2d comparison operator.
bool Vector2d::operator==(Vector2d& v)
{
	bool match;
	//as vectors containing floats will rarely, if ever be perfectly equal, a 1% tolerance is needed.
	if ((x-v.x) < 0.005f && (x-v.x) > -0.005f)
	{
		if ((y-v.y) < 0.005f && (y-v.y) > -0.005f)
			match = true;
		else
			match = false;
	}
	else 
		match = false;

	return match;
}
//returns a scaled up vector. current is unchanged.
const Vector2d Vector2d::operator* ( const float& s ) const
{
	return Vector2d(x*s,y*s);
}
//Dot product
const float Vector2d::dot( const Vector2d& v ) const
{
	return x*v.x + y*v.y;
}
//Length
const float Vector2d::length() const
{
	return sqrt((x*x)+(y*y));
}
//Unit vector
const Vector2d Vector2d::unit() const
{
	return Vector2d(x/length(),y/length());
}
//uses unit vector to normalise current.
void Vector2d::normalize()
{
	x=unit().x;
	y=unit().y;
}

