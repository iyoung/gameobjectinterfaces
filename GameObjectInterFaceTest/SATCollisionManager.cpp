#include "SATCollisionManager.h"
#include "Projection.h"

SATCollisionManager::SATCollisionManager(void)
{
}
bool SATCollisionManager::getCollision(Collidable& collidable1, Collidable& collidable2)
{
	bool collided;
	//algorithm
	//if two convex objects are not penetrating,
	//one or more axes exist for which the projection of the objects will not overlap
	//main algorithm
	//get axes for both collidable objects
	//for each axis in collidable1
	//project each collidable onto axis
	//check each projection for overlap
	//if no overlap
	//exit function returning false
	//repeat for loop for collidable2
	//if no exit condition met, return a collision as all axes have an overlap
	int numAxes1 = collidable1.getNumAxes();
	int numAxes2 = collidable2.getNumAxes();
	Vector2d* axes1 = new Vector2d[numAxes1];
	Vector2d* axes2 = new Vector2d[numAxes2];
	axes1 = collidable1.getAxes();
	axes2 = collidable2.getAxes();

	for (int i = 0; i < numAxes1; i++)
	{
		Vector2d axis = axes1[i];
		Projection p1 = collidable1.project(axis);
		Projection p2 = collidable2.project(axis);
		if(!p1.overlap(p2))
		{
			return false;
		}
	}
	for (int i = 0; i < numAxes2; i++)
	{
		Vector2d axis = axes2[i];
		Projection p1 = collidable1.project(axis);
		Projection p2 = collidable2.project(axis);
		if(!p1.overlap(p2))
		{
			return false;
		}
	}
	return true;
}

SATCollisionManager::~SATCollisionManager(void)
{
}
