#pragma once
#include "abstractcollisionmanager.h"

//collision detection will be possible for actual model data, but at increased computational cost
class SATCollisionManager :
	public AbstractCollisionManager
{
public:
	SATCollisionManager(void);
	virtual bool getCollision(Collidable& collidable1, Collidable& collidable2);
	~SATCollisionManager(void);
};

