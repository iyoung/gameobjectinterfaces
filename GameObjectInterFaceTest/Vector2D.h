// Vector2d.h : 2D Vector2d class interface file
// Daniel Livingstone
#include <fstream>
#include <iosfwd>

class Vector2d
{
public:
  float x; // x and y coordinates
  float y;
public:
	// constructor classes - these lines implement the constructors
	// so nothing more to do here.
	Vector2d() : x(0), y(0) { }
	Vector2d( float a, float b ) : x(a), y(b) {	}

	// Copy constructor goes here
	Vector2d(const Vector2d& v) : x(v.x), y(v.y) {}

	// You should try to implement as many of the following methods as possible
	//Compare Vector2d
	bool operator== ( Vector2d& v );
	//Perform negatation
	Vector2d operator- ();
	// Assignment operator
	const Vector2d& operator= ( const Vector2d& v );
	//Add Vector2d v to this Vector2d
	const Vector2d& operator+= ( const Vector2d& v );
	//Subtract Vector2d v from this Vector2d
	const Vector2d& operator-= ( const Vector2d& v );
	// Scale up - multiply by s
	const Vector2d& operator*= ( const float& s );
	// Scale down - divide by s
	const Vector2d& operator/= ( const float& s );
	// Add two Vector2d's - does not modify value of this Vector2d!
	const Vector2d operator+ ( const Vector2d& v ) const;
	// Subtract two Vector2ds - does not modify value of this Vector2d!
	const Vector2d operator- ( const Vector2d& v ) const;
	// Scale by a float - does not modify value of this Vector2d!
	// calculates: v * s
	const Vector2d operator* ( const float& s ) const;
	// Scale down by a float - does not modify value of this Vector2d!
	const Vector2d operator/ (float s) const;
	//Floating point dot product
	const float dot( const Vector2d& v ) const;
	// Length of this Vector2d
	const float length() const;
	// Return a unit Vector2d in same direction as this Vector2d
	const Vector2d unit() const;
	//make this a unit Vector2d
	void normalize();

	// Scale by a float - does not modify value of this Vector2d. Calculates: s * v
	// NOTE: this is a friend function and follows special rules
	friend inline const Vector2d operator* ( const float& s, const Vector2d& v ) {
		return v*s;
	}

	friend inline std::ostream& operator<< (std::ostream& out, const Vector2d& v){
		out	<<"x: "<<v.x<<", "
			<<"y: "<<v.y;
	return out;
	}
};

// define Point2D as an alias for Vector2d
typedef Vector2d Point2d;
