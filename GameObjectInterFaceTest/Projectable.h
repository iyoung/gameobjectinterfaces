#pragma once
#include "Projection.h"
#include "Vector2D.h"
class Projectable
{
public:
	Projectable(void);
	virtual Projection project(Vector2d& axis)=0;
	~Projectable(void);
};

