#pragma once
class Drawable
{
public:
	Drawable(void);
	virtual void draw()=0;
	virtual ~Drawable(void);
};

