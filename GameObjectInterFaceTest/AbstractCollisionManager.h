#pragma once
#include "Collidable.h"
//Collidable data type will ideally be realised as a very simple geometric shape (square, cube, octagon, etc)
//ComplexCollidable data type will extend Collidable interface, which will ideally
//be realised as a wrapper class for a collection of Collidable Objects
class AbstractCollisionManager
{
public:
	AbstractCollisionManager(void);
	virtual bool getCollision(Collidable& collidable1, Collidable& collidable2)=0;
	virtual bool getComplexCollision()=0;//will take in complexCollidable data type, for collections of objects 
	virtual ~AbstractCollisionManager(void);
};

