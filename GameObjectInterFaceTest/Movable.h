#pragma once
class Movable
{
public:
	Movable(void);
	virtual void setVelocity(int x, int y)=0;
	virtual void move()=0;
	virtual ~Movable(void);
};

