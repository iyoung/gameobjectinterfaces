#pragma once
#include "Collidable.h"
#include "Movable.h"
class AbstractPhysicsManager
{
public:
	AbstractPhysicsManager(void);
	virtual void collisionResponse(Collidable& collidable1, Collidable& collidable2)=0;
	virtual void applyPhysics(Movable& movable)=0;
	virtual ~AbstractPhysicsManager(void);
};

