#pragma once
class Projection
{
public:
	Projection(float Min, float Max);
	bool overlap(Projection& otherProjection);
	float min;
	float max;
	~Projection(void);
};

