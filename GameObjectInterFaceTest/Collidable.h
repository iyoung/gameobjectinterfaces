#pragma once
#include"Vector2D.h"
#include "Projectable.h"
class Collidable: public Projectable
{
public:
	Collidable(void);
	virtual Vector2d* getAxes()=0;
	virtual int getNumAxes()=0;
	virtual ~Collidable(void);
};

