#pragma once
#include "Drawable.h"
class AbstractRenderer
{
public:
	AbstractRenderer(void);
	virtual void render(Drawable& drawable)=0;
	virtual ~AbstractRenderer(void);
};

