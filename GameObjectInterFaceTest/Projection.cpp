#include "Projection.h"


Projection::Projection(float Min, float Max)
{
	min=Min;
	max = Max;
}

bool Projection::overlap(Projection& otherProjection)
{
	//check for overlap
	if(otherProjection.min>min && otherProjection.min<max)
		return true;
	if(otherProjection.max<max && otherProjection.max>min)
		return true;
	//check for containment
	if(min>otherProjection.min && min<otherProjection.max)
		return true;
	if(max<otherProjection.max && max > otherProjection.min)
		return true;
	//else no overlap
	else
		return false;
}
Projection::~Projection(void)
{
}
